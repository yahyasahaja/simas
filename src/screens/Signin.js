import React, { Component } from 'react'
import styled from 'styled-components'
import TextField from '@material-ui/core/TextField'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import Button from '@material-ui/core/Button'
import { user } from '../services/stores'

const Container = styled.div`
  display: flex;
  background: url("/images/kota-malang.jpg");
  background-size: cover;
  width: 100%;
  height: 100vh;
  position: relative;

  .shadow {
    background-color: black;
    opacity: .6;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .left {
    display: block;
    position: relative;
    width: 100%;
    padding-top: 100px;
    padding-left: 100px;

    .title {
      font-size: 50pt;
      color: white;
      z-index: 1;
      font-weight: 100;
    }

    .desc {
      font-weight: 100;
      color: white;
      font-size: 14pt;
    }
  }

  .right {
    padding: 40px;
    background-color: white;
    width: 100%;
    position: relative;
    max-width: 480px;
    display: flex;
    justify-content: center;
    align-items: center;

    h1 {
      text-align: center;
      font-weight: 300;
    }

    .button {
      margin-top: 20px;
    }
  }
`

@observer
class Signin extends Component {
  @observable username = ''
  @observable password = ''

  onSubmit = async e => {
    e.preventDefault()
    await user.login(this.username, this.password)
    if (user.isLoggedIn) this.props.history.push('/')
  }

  render() {
    return (
      <Container>
        <div className="shadow" />
        <div className="left" >
          <div className="title" >SIMAS</div>
          <div className="desc" >Sistem Informasi Manajemen ASN</div>
        </div>

        <div className="right" >
          <form onSubmit={this.onSubmit} className="wrapper" > 
            <h1>Sign In</h1>

            <TextField
              label="Username"
              margin="normal"
              variant="outlined"
              value={this.username}
              onChange={e => this.username = e.target.value}
              fullWidth
              type="text"
            />
            <TextField
              label="Password"
              margin="normal"
              variant="outlined"
              value={this.password}
              onChange={e => this.password = e.target.value}
              fullWidth
              type="password"
            />

            <Button 
              variant="outlined" 
              color="primary"
              fullWidth
              size="large"
              className="button"
              disabled={user.isLoadingLogin}
              type="submit"
              onClick={this.onSubmit}
            >
              Sign In
            </Button>
          </form>
        </div>
      </Container>
    )
  }
}

export default Signin