import React, { Component } from 'react'
// import { observable } from 'mobx'
import { observer } from 'mobx-react'
import styled from 'styled-components'
import Table from '../components/Table'
import pensiuns from '../services/stores/pensiuns'
import TextField from '@material-ui/core/TextField'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import MDIcon from '../components/MDIcon'
// import pensiun from '../services/stores/pangkats'

const ROWS = [
  { id: 'nama', numeric: false, disablePadding: false, label: 'Nama' },
  { id: 'nip', numeric: false, disablePadding: false, label: 'NIP' },
  // { id: 'tempat_lahir', numeric: false, disablePadding: false, label: 'Tempat Lahir' },
  // { id: 'tanggal_lahir', numeric: false, disablePadding: false, label: 'Tanggal Lahir' },
  // { id: 'pendidikan', numeric: false, disablePadding: false, label: 'Pendidikan' },
  { id: 'jabatan', numeric: false, disablePadding: false, label: 'Jabatan' },
  // { id: 'pangkat', numeric: false, disablePadding: false, label: 'Pangkat' },
  // { id: 'tmt_pangkat_terakhir', numeric: false, disablePadding: false, label: 'Tamat Pangkat' },
  { id: 'masa_kerja', numeric: false, disablePadding: false, label: 'Masa Kerja' },
  // { id: 'gaji_berkala', numeric: false, disablePadding: false, label: 'Gaji Berkala' },
  // { id: 'tmt_gaji_berkala_terakhir', numeric: false, disablePadding: false, label: 'Tamat Gaji Berkala' },
  // { 
  //   id: 'masa_kerja_gaji_berkala', 
  //   numeric: false, disablePadding: false, label: 'Masa Kerja Gaji Berkala' 
  // },
  { id: 'pensiun', numeric: false, disablePadding: false, label: 'Tanggal Pensiun' },
]

const Container = styled.div`
  display: block;
  width: 100%;
  background: #ecf0f1;
  min-height: 100vh;

  .card {
    background: white;
    margin: 10px 0;
    padding: 20px;
  }
`

@observer
class Notifications extends Component {
  timeoutId = -1
  
  renderPensiunsTable() {
    return (
      <Table 
        page={pensiuns.meta.page}
        onPageChange={page => {
          pensiuns.meta.page = page
          pensiuns.fetchPensiuns(page)
        }}
        isLoading={pensiuns.isLoading}
        data={pensiuns.data}
        meta={pensiuns.meta}
        rows={ROWS}
        rowsPerPage={10}
      />
    )
  }

  render() {
    return (
      <Container>
        <div className="card" >
          <h1>Pensiun</h1>
          <TextField
            variant="outlined"
            type="text"
            label="Search"
            value={pensiuns.search}
            onChange={e => {
              pensiuns.search = e.currentTarget.value
              pensiuns.meta.page = 0
              if (this.timeoutId !== -1) {
                clearTimeout(this.timeoutId)
              }

              this.timeoutId = setTimeout(() => {
                pensiuns.fetchPensiuns(pensiuns.meta.page)
                this.timeoutId = -1
              }, 1000)
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton edge="end">
                    <MDIcon icon="magnify" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          {this.renderPensiunsTable()}
        </div>
      </Container>
    )
  }
}

export default Notifications