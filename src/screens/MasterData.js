import React, { Component } from 'react'
import { observable, toJS } from 'mobx'
import { observer } from 'mobx-react'
import styled from 'styled-components'
import Table from '../components/Table'
import karyawans from '../services/stores/karyawans'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
// import DialogContentText from '@material-ui/core/DialogContentText'
import moment from 'moment'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import {
  KeyboardDatePicker,
} from '@material-ui/pickers'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import MDIcon from '../components/MDIcon'

const ROWS = [
  { id: 'nama', numeric: false, disablePadding: false, label: 'Nama' },
  { id: 'nip', numeric: false, disablePadding: false, label: 'NIP' },
  // { id: 'tempat_lahir', numeric: false, disablePadding: false, label: 'Tempat Lahir' },
  // { id: 'tanggal_lahir', numeric: false, disablePadding: false, label: 'Tanggal Lahir' },
  // { id: 'pendidikan', numeric: false, disablePadding: false, label: 'Pendidikan' },
  { id: 'jabatan', numeric: false, disablePadding: false, label: 'Jabatan' },
  { id: 'pangkat', numeric: false, disablePadding: false, label: 'Pangkat' },
  // { id: 'tmt_pangkat_terakhir', numeric: false, disablePadding: false, label: 'TMT Pangkat' },
  // { id: 'masa_kerja', numeric: false, disablePadding: false, label: 'Masa Kerja' },
  { id: 'gaji_berkala', numeric: false, disablePadding: false, label: 'Gaji Berkala' },
  // { id: 'tmt_gaji_berkala_terakhir', numeric: false, disablePadding: false, label: 'TMT Gaji Berkala' },
  // { 
  //   id: 'masa_kerja_gaji_berkala', 
  //   numeric: false, disablePadding: false, label: 'Masa Kerja Gaji Berkala' 
  // },
  // { id: 'pensiun', numeric: false, disablePadding: false, label: 'Tanggal Pensiun' },
]

const Container = styled.div`
  display: block;
  padding: 20px;
  width: 100%;

  .title {
    display: flex;
    justify-content: space-between;
  }
`

@observer
class MasterData extends Component {
  @observable isUpdateDialogOpened = false
  @observable isDeleteDialogOpened = false
  @observable karyawan = null
  @observable nama = ''
  @observable isNew = false
  @observable selectedKaryawanIds = []

  timeoutId = -1

  renderTable() {
    return (
      <Table 
        editable
        page={karyawans.meta.page}
        onPageChange={page => {
          karyawans.meta.page = page
          karyawans.fetchKaryawans(page)
        }}
        isLoading={karyawans.isLoading}
        data={karyawans.data}
        meta={karyawans.meta}
        rows={ROWS}
        rowsPerPage={10}
        onRowClick={(id, karyawan, karyawans) => {
          this.karyawan = toJS(karyawan)
          this.nama = karyawan.nama
          this.isUpdateDialogOpened = true
        }}
        onDelete={ids => {
          this.selectedKaryawanIds = ids
          this.isDeleteDialogOpened = true
        }}
        renderTopLeft={() => (
          <TextField
            variant="outlined"
            type="text"
            label="Search"
            value={karyawans.search}
            onChange={e => {
              karyawans.search = e.currentTarget.value
              karyawans.meta.page = 0
              if (this.timeoutId !== -1) {
                clearTimeout(this.timeoutId)
              }

              this.timeoutId = setTimeout(() => {
                karyawans.fetchKaryawans(karyawans.meta.page)
                this.timeoutId = -1
              }, 1000)
            }}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton edge="end">
                    <MDIcon icon="magnify" />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        )}
      />
    )
  }

  submit = async e => {
    e.preventDefault()
    if (this.isNew) {
      await karyawans.addKaryawan(toJS(this.karyawan))
    } else {
      await karyawans.updateKaryawan(this.karyawan.id, toJS(this.karyawan))
    }
    this.isUpdateDialogOpened = false
    karyawans.fetchKaryawans(karyawans.meta.page)
  }

  renderDialogs() {
    return (
      <React.Fragment>
        <Dialog 
        open={this.isUpdateDialogOpened} 
        onClose={() => this.isUpdateDialogOpened = false}
        fullWidth
        maxWidth="md"
      >
        <form onSubmit={this.submit} >
        {
          this.isUpdateDialogOpened && (
            <React.Fragment>
              <DialogTitle id="form-dialog-title">
                {this.isNew ? 'Create new' : `Update data for ${this.nama}`} 
              </DialogTitle>
              <DialogContent>
                <TextField
                  required
                  margin="normal"
                  autoFocus
                  label="Nama"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.nama}
                  onChange={e => this.karyawan.nama = e.target.value}
                />
                <TextField
                  required
                  margin="normal"
                  label="NIP"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.nip}
                  onChange={e => this.karyawan.nip = e.target.value}
                />
                <TextField
                  required
                  margin="normal"
                  label="Tempat Lahir"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.tempat_lahir}
                  onChange={e => this.karyawan.tempat_lahir = e.target.value}
                />
                <KeyboardDatePicker
                  required
                  fullWidth
                  disableToolbar
                  shouldDisableDate={date => {
                    let yesterday = moment(new Date()).subtract(1, 'd')
                    return moment(date).isSameOrAfter(yesterday)
                  }}
                  variant="inline"
                  format="YYYY-MM-DD"
                  margin="normal"
                  label="Tanggal Lahir"
                  value={this.karyawan.tanggal_lahir}
                  onChange={date => {
                    this.karyawan.tanggal_lahir = date.format('YYYY-MM-DD')
                  }}
                />
                <TextField
                  required
                  margin="normal"
                  label="Pendidikan"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.pendidikan}
                  onChange={e => this.karyawan.pendidikan = e.target.value}
                />
                <TextField
                  required
                  margin="normal"
                  label="Jabatan"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.jabatan}
                  onChange={e => this.karyawan.jabatan = e.target.value}
                />
                <TextField
                  required
                  margin="normal"
                  label="Pangkat"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.pangkat}
                  onChange={e => this.karyawan.pangkat = e.target.value}
                />
                <KeyboardDatePicker
                  required
                  fullWidth
                  disableToolbar
                  variant="inline"
                  format="YYYY-MM-DD"
                  margin="normal"
                  label="Tamat Pangkat Terakhir"
                  value={this.karyawan.tmt_pangkat_terakhir}
                  onChange={date => {
                    this.karyawan.tmt_pangkat_terakhir = date.format('YYYY-MM-DD')
                  }}
                />
                <TextField
                  required
                  margin="normal"
                  label="Masa Kerja"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.masa_kerja}
                  onChange={e => this.karyawan.masa_kerja = e.target.value}
                />
                <TextField
                  required
                  margin="normal"
                  label="Gaji Berkala"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.gaji_berkala}
                  onChange={e => this.karyawan.gaji_berkala = e.target.value}
                />
                <KeyboardDatePicker
                  fullWidth
                  required
                  disableToolbar
                  variant="inline"
                  format="YYYY-MM-DD"
                  margin="normal"
                  label="Tamat Gaji Berkala Terakhir"
                  value={this.karyawan.tmt_gaji_berkala_terakhir}
                  onChange={date => {
                    this.karyawan.tmt_gaji_berkala_terakhir = date.format('YYYY-MM-DD')
                  }}
                />
                <TextField
                  required
                  margin="normal"
                  label="Masa Kerja Gaji Berkala"
                  type="text"
                  fullWidth
                  variant="standard"
                  value={this.karyawan.masa_kerja_gaji_berkala}
                  onChange={e => this.karyawan.masa_kerja_gaji_berkala = e.target.value}
                />
                <KeyboardDatePicker
                  required
                  fullWidth
                  disableToolbar
                  variant="inline"
                  format="YYYY-MM-DD"
                  margin="normal"
                  label="Pensiun"
                  value={this.karyawan.pensiun}
                  onChange={date => {
                    this.karyawan.pensiun = date.format('YYYY-MM-DD')
                  }}
                />
              </DialogContent>
            </React.Fragment>
          )
        }
        <DialogActions>
          <Button onClick={() => {
            this.isUpdateDialogOpened = false
          }} color="primary">
            Cancel
          </Button>
          <Button type="submit" color="primary">
            {this.isNew ? 'Add' : 'Update'}
          </Button>
        </DialogActions>
        </form>
      </Dialog>
      <Dialog
        open={this.isDeleteDialogOpened}
        onClose={() => this.isDeleteDialogOpened = false}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        {this.isDeleteDialogOpened && (
          <React.Fragment>
            <DialogTitle>Delete Alert</DialogTitle>
            <DialogContent>
              Apakah anda yakin ingin menghapus karyawan?
            </DialogContent>
            <DialogActions>
              <Button onClick={() => this.isDeleteDialogOpened = false} color="secondary">
                Cancel
              </Button>
              <Button onClick={async () => {
                await karyawans.deleteKaryawan(this.selectedKaryawanIds)
                this.isDeleteDialogOpened = false
              }} color="primary" autoFocus>
                Delete
              </Button>
            </DialogActions>
          </React.Fragment>
        )}
      </Dialog>
      </React.Fragment>
    )
  }

  render() {
    return (
      <Container>
        <div className="title" >
          <h1>Data Karyawan</h1>
          <div>
          <IconButton onClick={() => {
            this.karyawan = {
              gaji_berkala: '',
              id: '',
              jabatan: '',
              masa_kerja: '',
              masa_kerja_gaji_berkala: '',
              nama: '',
              nip: '',
              pangkat: '',
              pendidikan: '',
              pensiun: null,
              tanggal_lahir: null,
              tempat_lahir: '',
              tmt_gaji_berkala_terakhir: null,
              tmt_pangkat_terakhir: null,
            }
            this.isNew = true
            this.isUpdateDialogOpened = true
          }} >
            <MDIcon 
              className="mdi mdi-account-plus"
            />
          </IconButton>
          </div>
        </div>
        {this.renderTable()}
        {this.renderDialogs()}
      </Container>
    )
  }
}

export default MasterData