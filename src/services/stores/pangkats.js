import { observable, action } from 'mobx'
import axios from 'axios'

class Pangkats {
  @observable data = []
  @observable meta = { page: 0, totalPages: 0, totalCount: 0 }
  @observable links = { prev: false, next: false}
  @observable limit = 10
  @observable isLoading = false
  @observable search = ''
  
  @action
  async fetchPangkats(page = 0, month = '04') {
    try {
      this.isLoading = true
      let {
        data: {
          data,
          meta,
          links
        }
      } = await axios.get(`/pangkat?page=${page}&limit=${this.limit}&month=${month}&search=${this.search}`)
      console.log(data, meta, links)

      this.data = data
      this.meta = meta
      this.links = links
      this.isLoading = false
    } catch (err) {
      this.isLoading = false
      console.log('response', err.response)
    }
  }
}

export default new Pangkats()