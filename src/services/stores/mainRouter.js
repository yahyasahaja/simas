import { observable, action } from 'mobx'

class MainRouter {
  @observable selectedPath = this.PATH_HOME
  @observable selectedRoute = null
  @observable containerPose = this.STATE_OPEN_CONTAINER

  PATH_MASTER_DATA = '/masterdata'
  PATH_PENSIUN = '/pensiun'
  PATH_TMT_PANGKAT_APRIL = '/tmtapril'
  PATH_TMT_PANGKAT_OKTOBER = '/tmtoktober'

  @action
  updateRoute(routers) {
    let path = window.location.pathname

    let route = routers.find(v => v.path === path)

    if (route) {
      this.selectedRoute = route
    }

    if (path.indexOf(this.PATH_MASTER_DATA) !== -1) {
      this.selectedPath = this.PATH_MASTER_DATA
    } else if (path.indexOf(this.PATH_PENSIUN) !== -1) {
      this.selectedPath = this.PATH_PENSIUN
    } else if (path.indexOf(this.PATH_TMT_PANGKAT_APRIL) !== -1) {
      this.selectedPath = this.PATH_TMT_PANGKAT_APRIL
    } else if (path.indexOf(this.PATH_TMT_PANGKAT_OKTOBER) !== -1) {
      this.selectedPath = this.PATH_TMT_PANGKAT_OKTOBER
    }
  }
}

export default window.mainRouter = new MainRouter()