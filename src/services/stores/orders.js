import { observable, action, toJS } from 'mobx'
import client from '../graphql/client'
import gql from 'graphql-tag'
import { user } from '.'

const URI_ORDERS = 'orders_uri_sans'

class Orders {
  @observable data = []
  @observable isFetchingOrders = false
  @observable unpaidOrders = []
  @observable processOrders = []
  @observable completedOrders = []
  @observable isFetchingOrder = false
  @observable order = null

  clear() {
    this.data = []
    this.unpaidOrders = []
    this.processOrders = []
    this.completedOrders = []
    this.order = null
    this.setLocalOrders(this.data)
  }

  @action
  async fetchOrder(id) {
    try {
      if (!this.order)  {
        this.isFetchingOrder = true
      }

      let {
        data: {
          order
        }
      } = await client.query({
        query: orderQuery,
        variables: {
          id
        }
      })

      this.order = order
      this.isFetchingOrder = false
      return order
    } catch (err) {
      this.isFetchingOrder = false
      console.log('ERROR FETCHING RESTAURANT', err.message)
    }
  }

  @action
  fetchOrdersFromLocal() {
    return JSON.parse(localStorage.getItem(URI_ORDERS))
  }

  @action
  setLocalOrders(orders) {
    localStorage.setItem(URI_ORDERS, JSON.stringify(toJS(orders)))
  }
  
  @action
  filterOrdersByStatus(status) {
    return this.data.filter(d => d.status === status)
  }

  @action
  initStatusedOrders() {
    this.unpaidOrders = this.data.filter(d => d.status === 'UNPAID')
    this.processOrders = this.data.filter(d => d.status === 'PROCESS')
    this.completedOrders = this.data.filter(d => d.status === 'COMPLETED')
  }

  @action
  async fetchOrders(status) {
    try {
      let localOrders = this.fetchOrdersFromLocal()
      if (localOrders && localOrders.length > 0) {
        this.data = localOrders
        this.initStatusedOrders()

        if (!user.isLoggedIn) return
      } else {
        this.isFetchingOrders = true
      }

      let variables
      if (status) variables.status = status

      let {
        data: {
          allOrders
        }
      } = await client.query({
        query: allOrdersQuery,
        fetchPolicy: 'network-only',
        variables
      })

      this.data = allOrders
      this.initStatusedOrders()
      this.setLocalOrders(allOrders)
      this.isFetchingOrders = false
      return allOrders
    } catch (err) {
      this.isFetchingOrders = false
      console.log('ERROR FETCHING RESTAURANT', err.message)
    }
  }
}

const allOrdersQuery = gql`
  query allOrders($status: OrderStatus) {
    allOrders(status: $status) {
      id
      total_price
      menu_price
      tax
      discount
      table_number
      status
      order_number
      created_at
      payment_method
      restaurant {
        id
        name
        slug
        picture
      }
      order_items {
        id
        quantity
        note
        restaurant_menu {
          id
          name
          description
          price
          image
          liked
        }
      }
    }
  }
`

const orderQuery = gql`
  query order($id: ID!) {
    order(id: $id) {
      id
      total_price
      menu_price
      tax
      discount
      table_number
      status
      order_number
      created_at
      payment_method
      restaurant {
        id
        name
        slug
        picture
      }
      order_items {
        id
        quantity
        note
        restaurant_menu {
          id
          name
          description
          price
          image
          liked
        }
      }
    }
  }
`

export default window.orders = new Orders()