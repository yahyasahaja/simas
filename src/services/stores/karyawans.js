import { observable, action } from 'mobx'
import axios from 'axios'
import snackbar from './snackbar'
import overlayLoading from './overlayLoading'

class Karyawans {
  @observable data = []
  @observable meta = { page: 0, totalPages: 0 }
  @observable links = { prev: false, next: false}
  @observable limit = 10
  @observable isLoading = false
  @observable search = ''
  
  @action
  async fetchKaryawans(page = 0) {
    try {
      this.isLoading = true
      let {
        data: {
          data,
          meta,
          links
        }
      } = await axios.get(`/karyawan?page=${page}&limit=${this.limit}&search=${this.search}`)

      this.data = data
      this.meta = meta
      this.links = links
      this.isLoading = false
    } catch (err) {
      this.isLoading = false
      console.log('response', err.response)
    }
  }

  @action
  async addKaryawan(karyawan) {
    try {
      overlayLoading.show()
      this.isLoading = true
      delete karyawan.id
      let {
        data: {
          is_ok
        }
      } = await axios.post(`/karyawan`, karyawan)

      if (is_ok) snackbar.show(`Karyawan ${karyawan.nama} added`)
      this.isLoading = false
      overlayLoading.hide()
    } catch (err) {
      snackbar.show(`Failed to update karyawan`)
      overlayLoading.hide()
      this.isLoading = false
      console.log('response', err.response)
    }
  }

  @action
  async updateKaryawan(id, karyawan) {
    try {
      overlayLoading.show()
      this.isLoading = true
      delete karyawan.id
      let {
        data: {
          is_ok
        }
      } = await axios.patch(`/karyawan/${id}`, karyawan)

      if (is_ok) snackbar.show(`Karyawan ${karyawan.nama} updated`)
      this.isLoading = false
      overlayLoading.hide()
    } catch (err) {
      snackbar.show(`Failed to update karyawan`)
      overlayLoading.hide()
      this.isLoading = false
      console.log('response', err.response)
    }
  }

  @action
  async deleteKaryawan(ids) {
    try {
      overlayLoading.show()
      this.isLoading = true
      for (let id of ids) {
        await axios.delete(`/karyawan/${id}`)
      }

      snackbar.show(`${ids.length} Karyawan deleted`)
      this.isLoading = false
      overlayLoading.hide()
    } catch (err) {
      snackbar.show(`Failed to update karyawan`)
      overlayLoading.hide()
      this.isLoading = false
      console.log('response', err.response)
    }
  }
}

export default new Karyawans()