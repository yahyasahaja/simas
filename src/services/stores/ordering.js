import { observable, computed, action, toJS } from 'mobx'
import user from './user'
import client from '../graphql/client'
import gql from 'graphql-tag'
// import { formatImageUrl } from '../../config'
import overlayLoading from './overlayLoading'
import snackbar from './snackbar'

class Ordering {
  @observable restaurant_id = null
  @observable order_items = []
  @observable payment_method = null
  // @observable customer_id = null
  @observable table_number = null
  @observable isCreatingOrder = false
  @observable slug = null
  @observable isFetchingRestaurant = false
  @observable restaurant = null

  @action
  reset() {
    this.restaurant_id = null
    this.order_items = []
    this.payment_method = null
    this.table_number = null
  }

  @computed
  get customer_id() {
    if (user.data) return user.data.id
    return null
  }

  @action
  getOrderItem(menu) {
    for (let i in this.order_items) {
      if (this.order_items[i].menu_id === menu.id) {
        return this.order_items[i]
      }
    }
  }

  @action
  addOrderItem = menu => {
    for (let i in this.order_items) {
      if (this.order_items[i].menu_id === menu.id) {
        this.order_items[i].quantity++
        return this.order_items[i]
      }
    }

    let orderItem = {
      quantity: 1,
      note: '',
      menu_id: menu.id,
      menu,
    }

    this.order_items.push(orderItem)
    return this.order_items[this.order_items.length - 1]
  }

  @action
  minOrderItem = menu => {
    for (let i in this.order_items) {
      if (this.order_items[i].menu_id === menu.id) {
        this.order_items[i].quantity--

        if (this.order_items[i].quantity <= 0) {
          this.order_items.splice(i, 1)
          //deleted
          return true
        }

        return false
      }
    }

    return false
  }

  @action
  changeOrderItemNote = (menu, note) => {
    for (let i in this.order_items) {
      if (this.order_items[i].menu_id === menu.id) {
        this.order_items[i].note = note
        return
      }
    }
  }

  @computed
  get estimatedPrice() {
    return this.order_items.reduce((prev, cur) => prev + cur.menu.price * cur.quantity, 0)
  }

  @action
  totalItems() {
    return this.order_items.reduce((prev, cur) => prev + cur.quantity, 0)
  }

  @action
  async fetchRestaurant() {
    try {
      this.isFetchingRestaurant = true
      let {
        data: {
          restaurant
        }
      } = await client.query({
        query: restaurantQuery,
        variables: {
          slug: this.slug
        }
      })

      // restaurant.picture = formatImageUrl(restaurant.picture)
      this.restaurant = restaurant
      this.restaurant_id = restaurant.id
      this.isFetchingRestaurant = false
      return restaurant
    } catch (err) {
      this.isFetchingRestaurant = false
      console.log('ERROR FETCHING RESTAURANT', err.response)
    }
  }
  
  @action
  async createOrder() {
    let {
      restaurant_id,
      order_items,
      payment_method,
      table_number,
      customer_id,
    } = this

    let orderItems = toJS(order_items)
    orderItems.forEach(d => delete d.menu)

    let variables = {
      restaurant_id,
      order_items: orderItems,
      payment_method,
      table_number,
      customer_id,
    }

    try {
      this.isCreatingOrder = true
      overlayLoading.show()
      let {
        data: {
          createOrder: order
        }
      } = await client.mutate({
        mutation: createOrderMutation,
        variables
      })

      this.isCreatingOrder = false
      overlayLoading.hide()
      snackbar.show('Order created!')
      return order
    } catch (err) {
      this.isCreatingOrder = false
      overlayLoading.hide()
      snackbar.show(err.message)
      console.log('ERROR WHILE CREATING ORDER', err.message)
    }
  }
}

const createOrderMutation = gql`
  mutation createOrder(
    $restaurant_id: ID!,
    $order_items: [OrderItemInput!]!,
    $payment_method: PaymentMethod!,
    $customer_id: ID!,
    $table_number: Int!,
  ) {
    createOrder(
      restaurant_id: $restaurant_id,
      order_items: $order_items,
      payment_method: $payment_method,
      customer_id: $customer_id,
      table_number: $table_number,
    ) {
      id
      total_price,
      table_number,
      # restaurant,
      # customer,
      # order_items,
      # order_number,
      status,
    }
  }
`

const restaurantQuery = gql`
  query restaurant($slug: String!) {
    restaurant(slug: $slug) {
      id
      slug
      name
      description
      opening_time
      closing_time
      is_24_hours
      address
      lat
      long
      picture
      categories {
        id
        name
        menus {
          id
          name
          description
          price
          image
          liked
        }
      }
    }
  }
`

export default window.ordering = new Ordering()