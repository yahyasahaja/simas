import { observable, action } from 'mobx'
import React from 'react'
import MDIcon from '../../components/MDIcon'

class Fab {
  @observable isActive = false 
  @observable bottomPosition = 60
  onClick = () => {}
  icon = <MDIcon icon="plus" />

  @action
  show() {
    this.isActive = true
  }

  @action
  hide() {
    this.isActive = false
  }
}

export default new Fab()