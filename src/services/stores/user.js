import { observable, computed, action } from 'mobx'
import axios from 'axios'
// import snackbar from './snackbar'
import token from './token'
import snackbar from './snackbar'

class User {
  @observable data = null
  @observable isLoading = false
  @observable isFetchingUsers = false
  @observable isLoadingLogin = false
  @observable isResendingEmail = false

  @computed
  get isLoggedIn() {
    return !!this.data
  }
  
  @action
  async login(username, password) {
    try {
      this.isLoadingLogin = true
      let {
        data: {
          token: accessToken
        }
      } = await axios.post('/auth/login', {
        username,
        password,
      })

      console.log(accessToken)

      token.setAccessToken(accessToken)
      this.data = accessToken
      this.isLoadingLogin = false
    } catch (err) {
      if (err.response) snackbar.show(err.response.data.message)
      this.isLoadingLogin = false
      console.log(err)
    }
  }

  @action
  logout = () => {
    localStorage.clear()
    this.data = null
  }
}
window.axios = axios
export default window.user = new User()