import React, { Component } from 'react'
import styled from 'styled-components'
import { formatMoney } from '../config'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import ListItem from '@material-ui/core/ListItem'
import moment from 'moment'
import { withRouter } from 'react-router-dom'

const Container = styled.div`
  display: flex;
  overflow: hidden;
  width: 100%;

  .list-item {
    padding: 0 10px;
    padding-top: 20px;
  }

  .picture-wrapper {
    display: block;
    margin-left: 10px;
    margin-right: 20px;

    .picture {
      border-radius: 10px;
      width: 75px;
      height: 75px;
      overflow: hidden;

      img {
        width: 75px;
        height: 75px;
        object-fit: cover;
        object-position: center;
      }
    }
  }

  .details {
    display: block;
    width: 100%;
    padding-bottom: 10px;
    border-bottom: 1px solid #eaeaea;
    min-height: 85px;

    .created-at {
      color: #22539c;
      font-size: 9pt;
    }

    .name {
      font-size: 12pt;
      font-weight: bold;
    }

    .items {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      width: 70%;
    }

    .total-price {
      font-size: 12pt;
    }
  }
`

class OrderCard extends Component {
  render() {
    let {
      restaurant: {
        name,
        picture,
      },
      order_items,
      total_price,
      created_at,
      id
    } = this.props.data

    let path = `/transactions/${id}`

    return (
        <Container onClick={() => this.props.history.push(path)} >
          <ListItem className="list-item" button >
            <div className="picture-wrapper" >
              <div className="picture" >
                <LazyLoadImage
                  alt="cover"
                  placeholderSrc="/images/flat-cover.jpg"
                  src={picture} 
                  effect="blur"
                />
              </div>
            </div>
            <div className="details" >
              <div className="created-at" >
                {moment(new Date(created_at)).format('DD MMM YYYY HH:mm')}
              </div>
              <div className="name" >{name}</div>
              <div className="items" >
                {order_items.reduce((prev, cur, i) => 
                  `${prev}${cur.restaurant_menu.name}: ${
                      cur.quantity
                  }${i === order_items.length - 1 ? '' : ', '}`, ''
                )}
              </div>
              <div className="total-price" >{formatMoney(total_price)}</div>
            </div>
          </ListItem>
      </Container>
    )
  }
}

export default withRouter(OrderCard)