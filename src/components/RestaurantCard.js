import React, { Component } from 'react'
import styled from 'styled-components'
// import { BASE_IMAGE_URL } from '../config'
import { withRouter } from 'react-router-dom'

const Container = styled.div`
  background: ${r => r.picture ? `url(${r.picture})` : 'black'};
  background-size: cover;
  background-position: center;
  position: relative;
  border-radius: 10px;
  height: 100px;
  overflow: hidden;
  margin: 10px 0;

  .shadow {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    opacity: .6;
    z-index: 1;
    background: black;
  }

  .name {
    font-size: 15pt;
    font-weight: bold;
    color: white;
    margin-top: 10px;
    margin-left: 20px;
    position: relative;
    z-index: 1;
  }
`

class RestaurantCard extends Component {
  render() {
    // let picture = `${BASE_IMAGE_URL}${this.props.restaurant.picture}`

    return (
      <Container 
        onClick={() => this.props.history.push(`/order/${this.props.restaurant.slug}`)} 
        picture={this.props.restaurant.picture} >
        <div className="shadow" />
        <div className="name" >{this.props.restaurant.name}</div>
      </Container>
    )
  }
}

export default withRouter(RestaurantCard)