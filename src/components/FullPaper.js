import React, { Component } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: block;
  padding: 15px;
  width: 100%;
  background: white;
  border-bottom: 1px solid #d5d9da;
  box-shadow: 0px 2px 4px #e4e8e8;
  margin-bottom: 10px;
`

export default class FullPaper extends Component {
  render() {
    return (
      <Container>
        {this.props.children}
      </Container>
    )
  }
}
