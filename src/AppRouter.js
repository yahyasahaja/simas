import React, { Component } from 'react'
import styled from 'styled-components'
import { Switch, Route, Redirect } from 'react-router-dom'
import { observer } from 'mobx-react'
// import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import MDIcon from './components/MDIcon'
import MasterData from './screens/MasterData'
import { mainRouter, token, user } from './services/stores'
import Pensiun from './screens/Pensiun'
import PangkatApril from './screens/PangkatApril'
import PangkatOktober from './screens/PangkatOktober'
import { observe, observable } from 'mobx'

const Container = styled.div`
  display: flex;
  min-height: 100vh;

  .logo {
    width: 100%;
    margin-bottom: 20px;
    display: flex;
    justify-content: center;

    img {
      width: 50%;
    }
  }
`

const Content = styled.div`
  display: block;
  width: calc(100% - 250px);
`

const StyledDrawer = styled.div`
  && {
    padding-top: 20px;
    border-right: 1px solid #e0e0e0;
    width: 250px;
  }

  .drawer-wrapper {
    position: sticky;
    top: 20px;
    width: 250px;
  }
`

const menus = [
  {
    label: 'Master Data ASN',
    icon: 'database',
    path: '/masterdata'
  },
  {
    label: 'Pensiun',
    icon: 'bell',
    path: '/pensiun'
  },
  {
    label: 'TMT Berkala April',
    icon: 'bell',
    path: '/tmtapril'
  },
  {
    label: 'TMT Berkala Oktober',
    icon: 'bell',
    path: '/tmtoktober'
  },
]

@observer
class AppRouter extends Component {
  @observable isLogoutDialogOpened = false

  componentWillReceiveProps(props) {
    if (this.props.location.pathname !== props.location.pathname) 
      mainRouter.updateRoute(menus)
  }

  componentDidMount() {
    mainRouter.updateRoute(menus)
    this.tokenDisposer = observe(token, 'isSettingUp', data => {
      if (data.oldValue === data.newValue) return
      if (!data.newValue && !user.isLoggedIn) {
        this.props.history.push('/signin')
      }
    })

    if (!token.isSettingUp) {
      if (!user.isLoggedIn) {
        this.props.history.push('/signin')
      }
    }
  }

  componentWillUnmount() {
    if (this.tokenDisposer) this.tokenDisposer()
  }

  render() {
    return (
      <Container>
        <StyledDrawer
          variant="permanent"
          anchor="left"
          className="drawer"
        >
          <div className="drawer-wrapper" >
            <div className="logo" >
              <img src="/images/logo.png" alt="" />
            </div>
            <List>
              {menus.map(({label, icon, path}, i) => (
                <ListItem 
                  onClick={() => this.props.history.push(path)}
                  selected={mainRouter.selectedPath === path} 
                  button key={i}>
                  <ListItemIcon><MDIcon icon={icon} /></ListItemIcon>
                  <ListItemText primary={label} />
                </ListItem>
              ))}
            </List>
            <Divider />
            <List>
              <ListItem button onClick={() => this.isLogoutDialogOpened = true} >
                <ListItemIcon><MDIcon icon="logout" /></ListItemIcon>
                <ListItemText primary="Logout" />
              </ListItem>
            </List>
          </div>
        </StyledDrawer>
        
        <Content>
          <Switch>
            <Route path="/masterdata" component={MasterData} />
            <Route path="/pensiun" component={Pensiun} />
            <Route path="/tmtapril" component={PangkatApril} />
            <Route path="/tmtoktober" component={PangkatOktober} />
            <Redirect from="*" to="/masterdata" />
          </Switch>
        </Content>

        <Dialog
          open={this.isLogoutDialogOpened}
          fullWidth
          onClose={() => {
            this.isLogoutDialogOpened = false
          }}
        >
          <DialogTitle>Logout Alert</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Continue logout?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.isLogoutDialogOpened = false} color="secondary">
              Nope
            </Button>
            <Button onClick={() => {
              user.logout()
              this.props.history.push('/signin')
            }} color="primary">
              Logout
            </Button>
          </DialogActions>
        </Dialog>
      </Container>
    )
  }
}

export default AppRouter